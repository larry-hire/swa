package com.tbl.shibwhalealerts.viewModel

import com.tbl.shibwhalealerts.service.model.data.TxData


interface FragmentCommunication {
    fun passData()
}